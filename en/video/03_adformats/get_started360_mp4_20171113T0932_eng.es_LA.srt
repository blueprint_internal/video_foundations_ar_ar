﻿1
00:00:00,645 --> 00:00:01,841
En Facebook,

2
00:00:01,841 --> 00:00:04,509
puedes sumergirte en los temas
y lugares que te interesan

3
00:00:04,509 --> 00:00:06,981
de un modo totalmente nuevo
en la sección de noticias.

4
00:00:06,981 --> 00:00:10,477
Estés donde estés, puedes experimentar
el mundo con solo dar un toque...

5
00:00:11,746 --> 00:00:13,079
deslizar el dedo...

6
00:00:14,619 --> 00:00:16,326
o rotar la pantalla del teléfono.

7
00:00:21,700 --> 00:00:24,151
Además, Facebook

8
00:00:24,151 --> 00:00:26,978
simplifica la forma de ver videos 360°
con Samsung Gear VR.

9
00:00:26,978 --> 00:00:28,415
Y como se trata de realidad virtual,

10
00:00:28,415 --> 00:00:31,572
puedes transportarte a un mundo
completamente nuevo en un instante.

11
00:00:36,766 --> 00:00:38,110
[Protegidos como parque nacional
desde 1999, la UNESCO declaró

12
00:00:38,110 --> 00:00:39,387
a los volcanes de Kamchatka como
Patrimonio de la Humanidad]

13
00:00:48,888 --> 00:00:51,022
Videos 360° en Facebook.

14
00:00:53,186 --> 00:00:54,217
[Facebook 360]

15
00:00:54,217 --> 00:00:56,015
[facebook360.fb.com]

