﻿1
00:00:00,645 --> 00:00:01,841
Auf Facebook kannst du

2
00:00:01,841 --> 00:00:04,509
direkt im News Feed auf eine komplett neue Art
in die Themen und Orte, eintauchen

3
00:00:04,509 --> 00:00:06,981
die dir wichtig sind.

4
00:00:06,981 --> 00:00:10,477
Egal, wo du gerade bist –
mit nur einer Berührung, ...

5
00:00:11,746 --> 00:00:13,079
einem Wischen ...

6
00:00:14,619 --> 00:00:16,326
oder einem Dreh kannst du die Welt entdecken.

7
00:00:21,700 --> 00:00:24,151
Außerdem lassen sich auf Facebook ganz einfach

8
00:00:24,151 --> 00:00:26,978
360°-Videos mit der
Samsung Gear VR betrachten.

9
00:00:26,978 --> 00:00:28,415
Mit VR kannst du dich

10
00:00:28,415 --> 00:00:31,572
im Handumdrehen in eine
neue Welt versetzen lassen.

11
00:00:35,000 --> 00:00:39,363
[Die Vulkane von Kamtschatka sind seit 1999 als Nationalpark
geschützt und gehören zum UNESCO-Weltkulturerbe]

12
00:00:48,888 --> 00:00:51,022
360°-Videos auf Facebook.

13
00:00:53,186 --> 00:00:54,217
facebook 360

14
00:00:54,217 --> 00:00:56,015
facebook360.fb.com

